let teamNames = ["ManchesterCity", "Liverpool", "Chelsea", "ManchesterUnited", "WestHam", "Arsenal", "Tottenham", "Wolverhamton",
    "Brighton", "Leicester", "AstonVilla", "Southampton", "Crystal", "Brentford", "LeedsUnited", "Everton"];

let standings = [];
let championList = [];
let winner;
let topTeam = [];
let placeCount= 1;

function makeTeamList() {
    for (i = 0; i < 16; i++) {
        let team = {};
        team.name = teamNames[i];
        team.power = Math.floor(Math.random() * 91) + 10;
        // team.power = Math.floor(Math.random() * 10)*10 + 10;
        standings.push(team);
    }
    console.log("Список команд :");
    console.log(standings);
}

function makeLottery(array) {
    for (let i = standings.length - 1; i >= 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [standings[i], standings[j]] = [standings[j], standings[i]];
        standings[i].index = i;
        standings[i].goals = 0;
    }
    console.log("Турнирная таблица :");
    console.log(standings);
}

function playTour(array) {
    championList=[];
    for (i = 0 , j = 1; i < array.length; i = i + 2, j++) {
        let result;
        let winnerInGroup;
        let looserInGroup;
        if (array[i].power === array[i + 1].power) {
            result = Math.round(Math.random());
            winnerInGroup = result === 0 ? array[i] : array[i + 1];
            looserInGroup = result === 1 ? array[i] : array[i+1];
        } else {
            let outsider = array[i].power > array[i + 1].power ? array[i + 1] : array[i];
            let favorite = array[i].power > array[i + 1].power ? array[i] : array[i + 1];
            result = Math.floor(Math.random() * (outsider.power + favorite.power + 1));
            winnerInGroup = result <= outsider.power ? outsider : favorite;
            looserInGroup = result >= outsider.power ? outsider : favorite;
        }
        let winnerInGroupGoals = Math.floor(Math.random() * 6) + 1;
        let looserInGroupGoals = Math.floor(Math.random() * winnerInGroupGoals);
        championList.push(winnerInGroup);
        console.log("Победитель группы " + j + ": " + winnerInGroup.name + " " +
            winnerInGroupGoals + ":" + looserInGroupGoals + " " + looserInGroup.name);

        standings[winnerInGroup.index].goals += winnerInGroupGoals;
        standings[looserInGroup.index].goals += looserInGroupGoals;
    }
    if (championList.length <= 1) {
        winner = championList[0];
        return console.log("Чемпион: " + winner.name);
    }
    console.log("Следующий раунд");
    console.log(championList);
    playTour(championList);
}

function sortByGoals (array) {
    array.sort((a, b) => a.goals < b.goals ? 1 : -1);
    console.log("Таблица по голам");

}
function findTopThree (array) {
    if(placeCount  > 3){
        return
    }
    console.log("Топ  " + placeCount );
    topTeam.push(...array.filter( elem => elem.goals === array[0].goals ));
    console.log(topTeam);
    standings.splice(0, topTeam.length);
    topTeam = [];
    placeCount++;
    findTopThree(standings);
}
function startGame() {
    makeTeamList();
    makeLottery(standings);
    playTour(standings);
    sortByGoals(standings);
    findTopThree(standings);
}

startGame();
